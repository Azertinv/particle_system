/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 08:18:21 by larrive           #+#    #+#             */
/*   Updated: 2016/02/20 15:31:44 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

int		main(void)
{
	double		last_time;
	double		time;
	double		delta_time;
	t_scene		scene;

	if (load_scene(&scene))
		return (-1);
	last_time = glfwGetTime();
	time = glfwGetTime();
	delta_time = 0.0;
	while (!glfwWindowShouldClose(scene.window))
	{
		time = glfwGetTime();
		delta_time = time - last_time;
		last_time = time;
		glfwPollEvents();
		update_scene(&scene, delta_time);
	}
	glfwTerminate();
	return (0);
}
