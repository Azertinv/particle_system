/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 08:18:02 by larrive           #+#    #+#             */
/*   Updated: 2016/02/20 15:23:33 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

GLuint		load_shader(GLenum shader_type, const char *filename)
{
	GLchar	*shader_source;
	int		fd;
	GLuint	shader;

	if (!(shader = glCreateShader(shader_type)))
		return (0);
	if ((fd = open(filename, O_RDONLY)) == -1)
		return (0);
	shader_source = ft_getfile(fd);
	glShaderSource(shader, 1, (const GLchar **)&shader_source, NULL);
	glCompileShader(shader);
	ft_putendl(filename);
	print_shader_log(shader);
	free(shader_source);
	return (shader);
}

GLuint		load_program(char *vs, char *gs, char *fs)
{
	GLuint	program;

	program = glCreateProgram();
	ft_putendl("Loaded shaders:");
	if (vs)
		glAttachShader(program, load_shader(GL_VERTEX_SHADER, vs));
	if (gs)
		glAttachShader(program, load_shader(GL_GEOMETRY_SHADER, gs));
	if (fs)
		glAttachShader(program, load_shader(GL_FRAGMENT_SHADER, fs));
	glLinkProgram(program);
	return (program);
}

void		print_shader_log(GLuint shader)
{
	GLchar		buffer[BUFF_SIZE];
	GLsizei		length;

	glGetShaderInfoLog(shader, BUFF_SIZE, &length, buffer);
	write(1, buffer, length);
}
