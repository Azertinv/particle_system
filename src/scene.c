/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/16 15:34:24 by larrive           #+#    #+#             */
/*   Updated: 2016/03/30 13:09:23 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

int			load_vao(t_scene *scene)
{
	glGenBuffers(1, &scene->bo_pos);
	glGenBuffers(1, &scene->bo_vel);
	glGenBuffers(1, &scene->bo_color);
	glGenVertexArrays(1, &scene->ao_system);
	glBindVertexArray(scene->ao_system);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, scene->bo_pos);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cl_float3) * MAX_PARTICLES,
			NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, scene->bo_vel);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cl_float3) * MAX_PARTICLES,
			NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, scene->bo_color);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cl_float3) * MAX_PARTICLES,
			NULL, GL_DYNAMIC_DRAW);
	return (0);
}

int			load_scene(t_scene *scene)
{
	if (!(scene->window = open_gl_init(WIDTH, HEIGHT, "Hello World")))
		return (-1);
	if (load_vao(scene))
		return (-1);
	if (load_opencl(scene, "./kernel/particle_system.cl"))
		return (-1);
	scene->shader_program = load_program("./shader/vertex_shader.glsl",
			NULL, "./shader/fragment_shader.glsl");
	scene->running = 0;
	scene->speed = 1.0;
	scene->world = translate(0.0, 0.0, 0.0);
	scene->cam = (t_camera){{0.0f, 5.0f, 15.0f}, {0.0f, 0.0f, 0.0f}};
	scene->projection = perspective(FOV, RATIO, NEAR, FAR);
	glProgramUniformMatrix4fv(scene->shader_program,
			glGetUniformLocation(scene->shader_program, "view2projection"),
			1, GL_FALSE, (GLfloat *)&(scene->projection));
	return (0);
}

double		get_average_delta(double delta_time)
{
	static double	last[10] = {0};
	double			average;
	int				i;

	i = -1;
	while (++i < 9)
		last[i] = last[i + 1];
	last[9] = delta_time;
	average = 0.0;
	i = -1;
	while (++i < 9)
		average += last[i];
	average /= 10;
	return (average);
}

void		display_info(t_scene *scene, double delta_time)
{
	char	title[100];
	char	*grav_state;
	double	fps;

	fps = 1 / get_average_delta(delta_time);
	grav_state = "OFF";
	if (scene->grav_state == GRAV_LINEAR)
		grav_state = "LINEAR";
	else if (scene->grav_state == GRAV_NEWTON)
		grav_state = "NEWTONIAN";
	snprintf(title, sizeof(title),
			"%.1lf FPS | PARTICLES %lu | GRAVITY %s | SPEED %.2f | %s\n",
			fps, scene->cl.count, grav_state, scene->speed,
			(scene->running ? "RUNNING" : "PAUSED"));
	glfwSetWindowTitle(scene->window, title);
}

void		get_mouse_vector(t_scene *scene, t_vector *external)
{
	t_vector	world_up;
	t_vector	up;
	t_vector	right;
	double		cursor_pos[2];

	ft_bzero(&world_up, sizeof(world_up));
	world_up.b = 1;
	glfwGetCursorPos(scene->window, cursor_pos, cursor_pos + 1);
	external->a = sin(scene->cam.dir[1]);
	external->b = -tan(scene->cam.dir[0]);
	external->c = -cos(scene->cam.dir[1]);
	external->d = 1;
	normalize((float *)external);
	right = cross(*external, world_up);
	normalize((float *)&right);
	up = cross(right, *external);
	normalize((float *)&up);
	*external = mxv(rotate_from_axis(up,
				-cos(cursor_pos[1] / HEIGHT * 1.28 - 1.28 / 2) * 1.35 *
				atan(cursor_pos[0] / WIDTH * 2 - 1)), *external);
	*external = mxv(rotate_from_axis(right,
				-atan(cursor_pos[1] / HEIGHT * 2 - 1)), *external);
	external->a = scene->cam.pos[0] + external->a * 50;
	external->b = scene->cam.pos[1] + external->b * 50;
	external->c = scene->cam.pos[2] + external->c * 50;
}

void		update_scene_state(t_scene *scene)
{
	static int	safety = 0;

	if (glfwGetKey(scene->window, GLFW_KEY_SPACE) == GLFW_RELEASE)
		safety = 0;
	else if (glfwGetKey(scene->window, GLFW_KEY_SPACE) == GLFW_PRESS && !safety)
	{
		safety = 1;
		scene->running = !scene->running;
	}
	if (glfwGetKey(scene->window, GLFW_KEY_Z) == GLFW_PRESS)
		scene->grav_state = GRAV_OFF;
	else if (glfwGetKey(scene->window, GLFW_KEY_X) == GLFW_PRESS)
		scene->grav_state = GRAV_LINEAR;
	else if (glfwGetKey(scene->window, GLFW_KEY_C) == GLFW_PRESS)
		scene->grav_state = GRAV_NEWTON;
	if (glfwGetKey(scene->window, GLFW_KEY_R) == GLFW_PRESS)
		scene->speed *= 1.01;
	else if (glfwGetKey(scene->window, GLFW_KEY_F) == GLFW_PRESS)
		scene->speed *= 0.99;
}

void		add_particle(t_scene *scene, t_vector pos)
{
	t_opencl_comp	*cl;
	int				amount;

	cl = &scene->cl;
	amount = 1000;
	clSetKernelArg(cl->particle_add, 0, sizeof(cl->pos), &cl->pos);
	clSetKernelArg(cl->particle_add, 1, sizeof(cl->vel), &cl->vel);
	clSetKernelArg(cl->particle_add, 2, sizeof(cl->color), &cl->color);
	clSetKernelArg(cl->particle_add, 3, sizeof(cl->count), &cl->count);
	clSetKernelArg(cl->particle_add, 4, sizeof(float) * 3, &pos);
	clSetKernelArg(cl->particle_add, 5, sizeof(amount), &amount);
	cl->count += amount;
	clEnqueueNDRangeKernel(cl->command, cl->particle_add, 1, NULL,
			&cl->count, NULL, 0, NULL, NULL);
}

void		update_particle(t_scene *scene, double delta_time)
{
	t_opencl_comp	*cl;
	float			delta;
	t_vector		external;

	cl = &scene->cl;
	delta = delta_time;
	get_mouse_vector(scene, &external);
	glFinish();
	clEnqueueAcquireGLObjects(cl->command, 3, &cl->pos, 0, NULL, NULL);
	if (glfwGetMouseButton(scene->window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
		clSetKernelArg(cl->particle_system, 5, sizeof(float) * 3, &external);
	if (glfwGetMouseButton(scene->window, GLFW_MOUSE_BUTTON_RIGHT) ==
			GLFW_PRESS)
		add_particle(scene, external);
	if (glfwGetKey(scene->window, GLFW_KEY_B) == GLFW_PRESS)
		init_particle_cube(&scene->cl);
	if (glfwGetKey(scene->window, GLFW_KEY_V) == GLFW_PRESS)
		init_particle_sphere(&scene->cl);
	clSetKernelArg(cl->particle_system, 3, sizeof(cl->count), &cl->count);
	clSetKernelArg(cl->particle_system, 4, sizeof(delta), &delta);
	clSetKernelArg(cl->particle_system, 6, sizeof(int), &(scene->running));
	clSetKernelArg(cl->particle_system, 7, sizeof(int), &(scene->grav_state));
	clSetKernelArg(cl->particle_system, 8, sizeof(float), &(scene->speed));
	clEnqueueNDRangeKernel(cl->command, cl->particle_system, 1, NULL,
			&cl->count, NULL, 0, NULL, NULL);
	clEnqueueReleaseGLObjects(cl->command, 3, &cl->pos, 0, NULL, NULL);
	clFinish(cl->command);
}

void		update_scene(t_scene *scene, double delta_time)
{
	update_scene_state(scene);
	update_camera(scene->window, &(scene->cam), delta_time);
	update_particle(scene, delta_time);
	display_info(scene, delta_time);
	scene->view = world2view(&scene->cam);
	glUniformMatrix4fv(glGetUniformLocation(scene->shader_program,
				"model2world"), 1, GL_FALSE, (GLfloat *)&(scene->world));
	glUniformMatrix4fv(glGetUniformLocation(scene->shader_program,
				"world2view"), 1, GL_FALSE, (GLfloat *)&(scene->view));
	glUniform3fv(glGetUniformLocation(scene->shader_program, "pos"),
			1, scene->cam.pos);
	glUniform3fv(glGetUniformLocation(scene->shader_program, "dir"),
			1, scene->cam.dir);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(scene->shader_program);
	glBindVertexArray(scene->ao_system);
	glDrawArrays(GL_POINTS, 0, scene->cl.count);
	glfwSwapBuffers(scene->window);
	if (glfwGetKey(scene->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(scene->window, 1);
}
