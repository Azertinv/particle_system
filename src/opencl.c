/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opencl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/13 11:26:49 by larrive           #+#    #+#             */
/*   Updated: 2016/03/10 17:02:16 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

static cl_program	load_cl_prog(t_opencl_comp *cl, char *filename)
{
	size_t		len;
	char		buffer[BUFF_SIZE];
	int			fd;
	cl_program	program;
	char		*kernel_source;

	ft_putendl(filename);
	if ((fd = open(filename, O_RDONLY)) == -1)
		return (0);
	kernel_source = ft_getfile(fd);
	close(fd);
	program = clCreateProgramWithSource(cl->context, 1,
			(const char **)&kernel_source, NULL, NULL);
	len = 0;
	if (program && clBuildProgram(program, 0, NULL, CL_BUILD_OP, NULL, NULL)
			!= CL_SUCCESS)
	{
		clGetProgramBuildInfo(program, cl->device_id, CL_PROGRAM_BUILD_LOG,
				BUFF_SIZE, buffer, &len);
		write(1, buffer, len);
		ft_putendl("failed to load cl program");
		return (0);
	}
	free(kernel_source);
	return (program);
}

void				init_particle_cube(t_opencl_comp *cl)
{
	clSetKernelArg(cl->particle_initc, 0, sizeof(cl->pos), &cl->pos);
	clSetKernelArg(cl->particle_initc, 1, sizeof(cl->vel), &cl->vel);
	clSetKernelArg(cl->particle_initc, 2, sizeof(cl->color), &cl->color);
	clSetKernelArg(cl->particle_initc, 3, sizeof(cl->count), &cl->count);
	clEnqueueNDRangeKernel(cl->command, cl->particle_initc, 1, NULL, &cl->count,
			NULL, 0, NULL, NULL);
}

void				init_particle_sphere(t_opencl_comp *cl)
{
	clSetKernelArg(cl->particle_inits, 0, sizeof(cl->pos), &cl->pos);
	clSetKernelArg(cl->particle_inits, 1, sizeof(cl->vel), &cl->vel);
	clSetKernelArg(cl->particle_inits, 2, sizeof(cl->color), &cl->color);
	clSetKernelArg(cl->particle_inits, 3, sizeof(cl->count), &cl->count);
	clEnqueueNDRangeKernel(cl->command, cl->particle_inits, 1, NULL, &cl->count,
			NULL, 0, NULL, NULL);
}

static void			load_fluid(t_scene *scene, t_opencl_comp *cl)
{
	cl_mem_flags	flags;
	float			external[3];

	cl->count = STARTING_PARTICLES;
	flags = CL_MEM_READ_WRITE;
	ft_bzero(external, sizeof(external));
	cl->pos = clCreateFromGLBuffer(cl->context, flags, scene->bo_pos, NULL);
	cl->vel = clCreateFromGLBuffer(cl->context, flags, scene->bo_vel, NULL);
	cl->color = clCreateFromGLBuffer(cl->context, flags, scene->bo_color, NULL);
	clSetKernelArg(cl->particle_system, 0, sizeof(cl->pos), &cl->pos);
	clSetKernelArg(cl->particle_system, 1, sizeof(cl->vel), &cl->vel);
	clSetKernelArg(cl->particle_system, 2, sizeof(cl->color), &cl->color);
	clSetKernelArg(cl->particle_system, 5, sizeof(external), external);
	cl->count = MAX_PARTICLES;
	init_particle_cube(cl);
	cl->count = STARTING_PARTICLES;
	clFinish(cl->command);
}

int					load_opencl(t_scene *scene, char *kernel_filename)
{
	t_opencl_comp			*cl;
	int						err;
	CGLShareGroupObj		cgl_sharegroup;
	cl_context_properties	props[3];

	cl = &(scene->cl);
	cgl_sharegroup = CGLGetShareGroup(CGLGetCurrentContext());
	props[0] = CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE;
	props[1] = (cl_context_properties)cgl_sharegroup;
	props[2] = 0;
	cl->context = clCreateContext(props, 0, NULL, NULL, NULL, &err);
	err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &cl->device_id, NULL);
	if (err != CL_SUCCESS)
		return (1);
	if (!cl->context)
		return (1);
	cl->command = clCreateCommandQueue(cl->context, cl->device_id, 0, &err);
	if (!cl->command)
		return (1);
	ft_putendl("Loaded cl program:");
	if (!(cl->program = load_cl_prog(cl, kernel_filename)))
		return (1);
	cl->particle_system = clCreateKernel(cl->program, "particle_system", &err);
	cl->particle_initc = clCreateKernel(cl->program, "particle_initc", &err);
	cl->particle_inits = clCreateKernel(cl->program, "particle_inits", &err);
	cl->particle_add = clCreateKernel(cl->program, "particle_add", &err);
	if (!cl->particle_system || !cl->particle_initc || !cl->particle_add)
		return (1);
	load_fluid(scene, cl);
	return (0);
}
