/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projection.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/15 11:06:30 by larrive           #+#    #+#             */
/*   Updated: 2016/03/02 12:59:22 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

t_matrix	perspective(float fov, float aspect, float near, float far)
{
	t_matrix	ret;
	float		f;

	ft_bzero(&ret, sizeof(ret));
	f = 1.0f / (float)(tan(fov / 2.0f * (M_PI / 180.0f)));
	ret.a = f / aspect;
	ret.f = f;
	ret.k = (near + far) / (near - far);
	ret.l = 2.0f * near * far / (near - far);
	ret.o = -1.0f;
	return (ret);
}

t_matrix	world2view(t_camera *cam)
{
	t_matrix	ret;

	ret = rotate_x(cam->dir[0]);
	ret = mxm(ret, rotate_y(cam->dir[1]));
	ret = mxm(ret, rotate_z(cam->dir[2]));
	ret = mxm(ret, translate(-cam->pos[0], -cam->pos[1], -cam->pos[2]));
	return (ret);
}
