/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   particle.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 15:21:16 by larrive           #+#    #+#             */
/*   Updated: 2016/03/10 18:40:06 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARTICLE_H
# define PARTICLE_H

# include <GL/glew.h>
# include <GLFW/glfw3.h>
# include <OpenCL/opencl.h>
# include <unistd.h>
# include <fcntl.h>
# include <math.h>
# include <libft.h>
# include <stdio.h>
# include "ft_matrix.h"

# define NEAR 0.01f
# define FAR 1000.0f
# define FOV 90.0f
# define RATIO (16.0f / 9.0f)
# define HEIGHT 1080
# define WIDTH 1920
# define BUFF_SIZE 32000
# define RATE_YAW 3.0f
# define RATE_PITCH 3.0f
# define RATE_FORWARD 10.0f
# define RATE_BACKWARD 10.0f
# define RATE_LEFT 10.0f
# define RATE_RIGHT 10.0f
# define RATE_UP 10.0f
# define RATE_DOWN 10.0f
# define STARTING_PARTICLES 1000000
# define MAX_PARTICLES 10000000
# define CL_BUILD_OP ("-cl-fast-relaxed-math")

# define GRAV_OFF 0
# define GRAV_LINEAR 1
# define GRAV_NEWTON 2

typedef struct	s_camera
{
	GLfloat		pos[3];
	GLfloat		dir[3];
}				t_camera;

typedef struct	s_opencl_comp
{
	cl_device_id		device_id;
	cl_context			context;
	cl_command_queue	command;
	cl_program			program;
	cl_kernel			particle_system;
	cl_kernel			particle_initc;
	cl_kernel			particle_inits;
	cl_kernel			particle_add;
	cl_mem				pos;
	cl_mem				vel;
	cl_mem				color;
	size_t				count;
}				t_opencl_comp;

typedef struct	s_scene
{
	GLFWwindow		*window;
	GLuint			shader_program;
	t_matrix		world;
	t_camera		cam;
	t_matrix		view;
	t_matrix		projection;
	t_opencl_comp	cl;
	int				running;
	int				grav_state;
	float			speed;
	GLuint			bo_pos;
	GLuint			bo_vel;
	GLuint			bo_color;
	GLuint			ao_system;
}				t_scene;

/*
** TO REMOVE
*/
CGLContextObj	CGLGetCurrentContext(void);

/*
** OPENGL
*/
GLFWwindow		*open_gl_init(int width, int height, const char *win_name);

/*
** SHADER
*/
GLuint			load_shader(GLenum shader_type, const char *filename);
GLuint			load_program(char *vs, char *gs, char *fs);
void			print_shader_log(GLuint shader);

/*
** OPENCL
*/
void			init_particle_cube(t_opencl_comp *cl);
void			init_particle_sphere(t_opencl_comp *cl);
int				load_opencl(t_scene *scene, char *kernel_filename);

/*
** MATRIX
*/
t_matrix		perspective(float fov, float aspect, float near, float far);
t_matrix		world2view(t_camera *cam);

/*
** SCENE
*/
void			normalize(GLfloat *tab);
t_matrix		rotate_from_axis(t_vector axis, float angle);
t_vector		cross(t_vector a, t_vector b);
int				load_scene(t_scene *scene);
void			update_scene(t_scene *scene, double delta_time);
void			update_camera(GLFWwindow *window, t_camera *cam, double d_time);

#endif
