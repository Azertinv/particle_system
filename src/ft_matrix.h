/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matrix.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 10:57:29 by larrive           #+#    #+#             */
/*   Updated: 2015/10/15 10:38:05 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MATRIX_H
# define FT_MATRIX_H

# include <stdlib.h>
# include <libft.h>

typedef struct	s_matrix
{
	float		a;
	float		b;
	float		c;
	float		d;
	float		e;
	float		f;
	float		g;
	float		h;
	float		i;
	float		j;
	float		k;
	float		l;
	float		m;
	float		n;
	float		o;
	float		p;
}				t_matrix;

typedef struct	s_vector
{
	float		a;
	float		b;
	float		c;
	float		d;
	int			color;
}				t_vector;

t_matrix		*new_matrix(t_matrix *mat);
t_vector		*new_vector(t_vector *vec);

t_matrix		mxm(t_matrix mat1, t_matrix mat2);
t_vector		mxv(t_matrix m, t_vector v);

t_matrix		rotate_x(float beta);
t_matrix		rotate_y(float beta);
t_matrix		rotate_z(float beta);
t_matrix		translate(float x, float y, float z);
t_matrix		scale(float x, float y, float z);

#endif
