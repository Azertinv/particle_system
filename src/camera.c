/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/16 07:58:51 by larrive           #+#    #+#             */
/*   Updated: 2016/03/30 12:49:05 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "particle.h"

void		normalize(GLfloat *tab)
{
	float	root;

	root = sqrt(tab[0] * tab[0] + tab[1] * tab[1] + tab[2] * tab[2]);
	if (root)
	{
		tab[0] /= root;
		tab[1] /= root;
		tab[2] /= root;
	}
}

t_matrix	rotate_from_axis(t_vector axis, float angle)
{
	float		s;
	float		c;
	float		oc;
	t_matrix	ret;

	normalize((float *)&axis);
	s = sin(angle);
	c = cos(angle);
	oc = 1.0 - c;
	ft_bzero(&ret, sizeof(ret));
	ret.a = oc * axis.a * axis.a + c;
	ret.b = oc * axis.a * axis.b - axis.c * s;
	ret.c = oc * axis.c * axis.a + axis.b * s;
	ret.e = oc * axis.a * axis.b + axis.c * s;
	ret.f = oc * axis.b * axis.b + c;
	ret.g = oc * axis.b * axis.c - axis.a * s;
	ret.i = oc * axis.c * axis.a - axis.b * s;
	ret.j = oc * axis.b * axis.c + axis.a * s;
	ret.k = oc * axis.c * axis.c + c;
	ret.p = 1.0;
	return (ret);
}

t_vector	cross(t_vector a, t_vector b)
{
	t_vector	ret;

	ret.a = a.b * b.c - a.c * b.b;
	ret.b = a.c * b.a - a.a * b.c;
	ret.c = a.a * b.b - a.b * b.a;
	return (ret);
}

static void	get_cam_direction(GLFWwindow *window, t_camera *cam,
			GLfloat *tab)
{
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		tab[0] = sin(cam->dir[1] + M_PI / 2);
		tab[2] = -cos(cam->dir[1] + M_PI / 2);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		tab[0] = -sin(cam->dir[1] + M_PI / 2);
		tab[2] = cos(cam->dir[1] + M_PI / 2);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		tab[0] = -sin(cam->dir[1]);
		tab[1] = tan(cam->dir[0]);
		tab[2] = cos(cam->dir[1]);
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		tab[0] = sin(cam->dir[1]);
		tab[1] = -tan(cam->dir[0]);
		tab[2] = -cos(cam->dir[1]);
	}
}

void		update_camera(GLFWwindow *window, t_camera *cam, double delta_time)
{
	GLfloat		next_pos[3];

	ft_bzero(next_pos, sizeof(next_pos));
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		cam->pos[1] += delta_time * RATE_UP;
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		cam->pos[1] -= delta_time * RATE_DOWN;
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		cam->dir[1] -= delta_time * RATE_YAW;
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		cam->dir[1] += delta_time * RATE_YAW;
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		cam->dir[0] += delta_time * RATE_PITCH;
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		cam->dir[0] -= delta_time * RATE_PITCH;
	if (cam->dir[0] >= M_PI / 2)
		cam->dir[0] = M_PI / 2 - 0.000001;
	if (cam->dir[0] <= -M_PI / 2)
		cam->dir[0] = -M_PI / 2 + 0.000001;
	get_cam_direction(window, cam, next_pos);
	normalize(next_pos);
	cam->pos[0] += next_pos[0] * delta_time * RATE_FORWARD;
	cam->pos[1] += next_pos[1] * delta_time * RATE_FORWARD;
	cam->pos[2] += next_pos[2] * delta_time * RATE_FORWARD;
}
