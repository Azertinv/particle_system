/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matrix_gen.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 18:26:22 by larrive           #+#    #+#             */
/*   Updated: 2015/10/15 10:48:19 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "ft_matrix.h"
#include "libft.h"

t_matrix	rotate_x(float beta)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.f = cos(beta);
	ret.g = -sin(beta);
	ret.j = sin(beta);
	ret.k = cos(beta);
	ret.a = 1.0;
	ret.p = 1.0;
	return (ret);
}

t_matrix	rotate_y(float beta)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.a = cos(beta);
	ret.c = sin(beta);
	ret.i = -sin(beta);
	ret.k = cos(beta);
	ret.f = 1.0;
	ret.p = 1.0;
	return (ret);
}

t_matrix	rotate_z(float beta)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.a = cos(beta);
	ret.b = -sin(beta);
	ret.e = sin(beta);
	ret.f = cos(beta);
	ret.k = 1.0;
	ret.p = 1.0;
	return (ret);
}

t_matrix	translate(float x, float y, float z)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.a = 1.0;
	ret.d = x;
	ret.f = 1.0;
	ret.h = y;
	ret.k = 1.0;
	ret.l = z;
	ret.p = 1.0;
	return (ret);
}

t_matrix	scale(float x, float y, float z)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.a = x;
	ret.f = y;
	ret.k = z;
	ret.p = 1.0;
	return (ret);
}
