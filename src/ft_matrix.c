/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matrix.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 10:56:55 by larrive           #+#    #+#             */
/*   Updated: 2015/10/15 10:38:03 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_matrix.h"

t_matrix	*new_matrix(t_matrix *mat)
{
	t_matrix	*ret;

	if (mat)
	{
		if (!(ret = (t_matrix *)malloc(sizeof(t_matrix))))
			exit(-1);
		ft_memcpy(ret, mat, sizeof(t_matrix));
	}
	else
	{
		if (!(ret = (t_matrix *)ft_memalloc(sizeof(t_matrix))))
			exit(-1);
	}
	return (ret);
}

t_vector	*new_vector(t_vector *vec)
{
	t_vector	*ret;

	if (vec)
	{
		if (!(ret = (t_vector *)malloc(sizeof(t_vector))))
			exit(-1);
		ft_memcpy(ret, vec, sizeof(t_vector));
	}
	else
	{
		if (!(ret = (t_vector *)ft_memalloc(sizeof(t_vector))))
			exit(-1);
	}
	return (ret);
}

t_matrix	mxm(t_matrix m1, t_matrix m2)
{
	t_matrix	ret;

	ft_bzero(&ret, sizeof(t_matrix));
	ret.a = m1.a * m2.a + m1.b * m2.e + m1.c * m2.i + m1.d * m2.m;
	ret.b = m1.a * m2.b + m1.b * m2.f + m1.c * m2.j + m1.d * m2.n;
	ret.c = m1.a * m2.c + m1.b * m2.g + m1.c * m2.k + m1.d * m2.o;
	ret.d = m1.a * m2.d + m1.b * m2.h + m1.c * m2.l + m1.d * m2.p;
	ret.e = m1.e * m2.a + m1.f * m2.e + m1.g * m2.i + m1.h * m2.m;
	ret.f = m1.e * m2.b + m1.f * m2.f + m1.g * m2.j + m1.h * m2.n;
	ret.g = m1.e * m2.c + m1.f * m2.g + m1.g * m2.k + m1.h * m2.o;
	ret.h = m1.e * m2.d + m1.f * m2.h + m1.g * m2.l + m1.h * m2.p;
	ret.i = m1.i * m2.a + m1.j * m2.e + m1.k * m2.i + m1.l * m2.m;
	ret.j = m1.i * m2.b + m1.j * m2.f + m1.k * m2.j + m1.l * m2.n;
	ret.k = m1.i * m2.c + m1.j * m2.g + m1.k * m2.k + m1.l * m2.o;
	ret.l = m1.i * m2.d + m1.j * m2.h + m1.k * m2.l + m1.l * m2.p;
	ret.m = m1.m * m2.a + m1.n * m2.e + m1.o * m2.i + m1.p * m2.m;
	ret.n = m1.m * m2.b + m1.n * m2.f + m1.o * m2.j + m1.p * m2.n;
	ret.o = m1.m * m2.c + m1.n * m2.g + m1.o * m2.k + m1.p * m2.o;
	ret.p = m1.m * m2.d + m1.n * m2.h + m1.o * m2.l + m1.p * m2.p;
	return (ret);
}

t_vector	mxv(t_matrix m, t_vector v)
{
	t_vector	ret;

	ft_bzero(&ret, sizeof(t_vector));
	ret.a = v.a * m.a + v.b * m.b + v.c * m.c + v.d * m.d;
	ret.b = v.a * m.e + v.b * m.f + v.c * m.g + v.d * m.h;
	ret.c = v.a * m.i + v.b * m.j + v.c * m.k + v.d * m.l;
	ret.d = v.a * m.m + v.b * m.n + v.c * m.o + v.d * m.p;
	ret.color = v.color;
	return (ret);
}
