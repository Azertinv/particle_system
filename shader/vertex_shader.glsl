#version 400

layout(location = 0) in vec3 particle_pos;
layout(location = 1) in vec3 particle_vel;
layout(location = 2) in vec3 particle_color;

out vec3		color;

uniform mat4	model2world = mat4(1);
uniform vec3	pos = vec3(0, 0, 2);
uniform vec3	dir = vec3(0, 0, 0);
uniform mat4	world2view = mat4(1);
uniform mat4	view2projection = mat4(1);

mat4	rotate_mat(vec3 vec);
mat4	translate_mat(vec3 vec);
mat4	scale_mat(vec3 vec);


void main() {
	gl_Position = vec4(particle_pos, 1.0) * model2world * world2view * view2projection;
	color = particle_color;
}
