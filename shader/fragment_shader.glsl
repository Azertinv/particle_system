#version 400

in	vec3	color;

out	vec4	frag_colour;

void main() {

	frag_colour.xyz = color.xyz;
	frag_colour.w = 0.9f;
}
