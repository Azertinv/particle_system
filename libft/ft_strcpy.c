/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 17:27:59 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:33:17 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	int i;

	if (!dest || !src)
		return (NULL);
	i = 0;
	while ((dest[i] = src[i]) != '\0')
		i++;
	return (dest);
}
