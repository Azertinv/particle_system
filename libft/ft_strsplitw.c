/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplitw.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 11:11:49 by larrive           #+#    #+#             */
/*   Updated: 2015/01/19 11:13:49 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	split_tab_size(const char *s)
{
	while (*s && ft_iswhite(*s))
		s++;
	if (!*s)
		return (0);
	while (*s && !ft_iswhite(*s))
		s++;
	return (1 + split_tab_size(s));
}

static size_t	split_elem_size(const char *s)
{
	size_t	size;

	size = 0;
	while (*s && !ft_iswhite(*s))
	{
		s++;
		size++;
	}
	return (size);
}

char			**ft_strsplitw(char const *s)
{
	char	**tab;
	size_t	elem_size;
	size_t	tab_len;
	size_t	i;

	if (!s)
		return (NULL);
	tab_len = split_tab_size(s);
	tab = (char **)malloc(sizeof(char *) * (tab_len + 1));
	if (!tab)
		return (NULL);
	i = 0;
	while (i < tab_len)
	{
		while (*s && ft_iswhite(*s))
			s++;
		elem_size = split_elem_size(s);
		tab[i] = ft_memcpy(ft_strnew(elem_size), s, elem_size);
		s += elem_size + 1;
		i++;
	}
	tab[tab_len] = NULL;
	return (tab);
}
