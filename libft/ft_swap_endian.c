/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_endian.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 14:03:03 by rpereira          #+#    #+#             */
/*   Updated: 2015/03/21 14:21:57 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned long int	swap_endian_64(unsigned long int x)
{
	x = (x & 0x00000000FFFFFFFF) << 32 | (x & 0xFFFFFFFF00000000) >> 32;
	x = (x & 0x0000FFFF0000FFFF) << 16 | (x & 0xFFFF0000FFFF0000) >> 16;
	x = (x & 0x00FF00FF00FF00FF) << 8 | (x & 0xFF00FF00FF00FF00) >> 8;
	return (x);
}

unsigned int		swap_endian_32(unsigned int num)
{
	num = ((num >> 24) & 0xff) | ((num << 8) & 0xff0000) | ((num >> 8)
			& 0xff00) | ((num << 24) & 0xff000000);
	return (num);
}

unsigned short		swap_endian_16(unsigned short num)
{
	num = (num >> 8) | (num << 8);
	return (num);
}
