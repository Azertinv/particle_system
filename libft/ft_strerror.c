/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strerror.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/17 11:06:08 by larrive           #+#    #+#             */
/*   Updated: 2015/03/17 11:27:30 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_err_info	g_error_table[] =
{
	FT_ENTRY(EPERM, "EPERM", "Not owner"),
	FT_ENTRY(ENOENT, "ENOENT", "No such file or directory"),
	FT_ENTRY(ESRCH, "ESRCH", "No such process"),
	FT_ENTRY(EINTR, "EINTR", "Interrupted system call"),
	FT_ENTRY(EIO, "EIO", "I/O error"),
	FT_ENTRY(ENXIO, "ENXIO", "No such device or address"),
	FT_ENTRY(E2BIG, "E2BIG", "Arg list too long"),
	FT_ENTRY(ENOEXEC, "ENOEXEC", "Exec format error"),
	FT_ENTRY(EBADF, "EBADF", "Bad file number"),
	FT_ENTRY(ECHILD, "ECHILD", "No child processes"),
	FT_ENTRY(EAGAIN, "EAGAIN", "No more processes"),
	FT_ENTRY(ENOMEM, "ENOMEM", "Not enough space"),
	FT_ENTRY(EACCES, "EACCES", "Permission denied"),
	FT_ENTRY(EFAULT, "EFAULT", "Bad address"),
	FT_ENTRY(EBUSY, "EBUSY", "Device busy"),
	FT_ENTRY(EEXIST, "EEXIST", "File exists"),
	FT_ENTRY(EXDEV, "EXDEV", "Cross-device link"),
	FT_ENTRY(ENODEV, "ENODEV", "No such device"),
	FT_ENTRY(ENOTDIR, "ENOTDIR", "Not a directory"),
	FT_ENTRY(EISDIR, "EISDIR", "Is a directory"),
	FT_ENTRY(EINVAL, "EINVAL", "Invalid argument"),
	FT_ENTRY(ENFILE, "ENFILE", "File table overflow"),
	FT_ENTRY(EMFILE, "EMFILE", "Too many open files"),
	FT_ENTRY(ENOTTY, "ENOTTY", "Not a typewriter"),
	FT_ENTRY(EFBIG, "EFBIG", "File too large"),
	FT_ENTRY(ENOSPC, "ENOSPC", "No space left on device"),
	FT_ENTRY(ESPIPE, "ESPIPE", "Illegal seek"),
	FT_ENTRY(EROFS, "EROFS", "Read-only file system"),
	FT_ENTRY(EMLINK, "EMLINK", "Too many links"),
	FT_ENTRY(EPIPE, "EPIPE", "Broken pipe"),
	FT_ENTRY(EDOM, "EDOM", "Math argument out of domain of func"),
	FT_ENTRY(ERANGE, "ERANGE", "Math result not representable"),
	FT_ENTRY(EDEADLK, "EDEADLK", "Deadlock condition"),
	FT_ENTRY(ENOLCK, "ENOLCK", "No record locks available"),
	FT_ENTRY(EBADMSG, "EBADMSG", "Not a data message"),
	FT_ENTRY(ENAMETOOLONG, "ENAMETOOLONG", "File name too long"),
	FT_ENTRY(EILSEQ, "EILSEQ", "Illegal byte sequence"),
	FT_ENTRY(ENOSYS, "ENOSYS", "Operation not applicable"),
	FT_ENTRY(ENOTEMPTY, "ENOTEMPTY", "Directory not empty"),
	FT_ENTRY(EMSGSIZE, "EMSGSIZE", "Message too long"),
	FT_ENTRY(ETIMEDOUT, "ETIMEDOUT", "Connection timed out"),
	FT_ENTRY(EINPROGRESS, "EINPROGRESS", "Operation now in progress"),
	FT_ENTRY(0, 0, "Undefined error")
};

char	*ft_strerror(int errnum)
{
	int		i;

	i = 0;
	while (g_error_table[i].name && g_error_table[i].value != errnum)
		i++;
	return (g_error_table[i].msg);
}
