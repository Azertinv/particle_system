/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_basetoi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 13:22:57 by larrive           #+#    #+#             */
/*   Updated: 2015/01/10 13:47:01 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_basetoi(const char *str, const char *base)
{
	int		result;
	int		negative;
	int		base_len;

	if (!str || !base)
		return (0);
	negative = 0;
	result = 0;
	if ((base_len = ft_strlen(base)) == 0)
		return (0);
	while (*str && ft_iswhite(*str))
		str++;
	if (*str == '-')
		negative = (long int)str++;
	else if (*str == '+')
		str++;
	while (*str && ft_is(*str, base))
	{
		result *= base_len;
		result += ft_indexof(*str, base);
		str++;
	}
	return ((negative ? -result : result));
}
