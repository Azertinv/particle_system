/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 14:08:42 by rpereira          #+#    #+#             */
/*   Updated: 2015/10/14 09:47:36 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <errno.h>
# include <fcntl.h>
# include <sys/stat.h>

# define ABS(x) ((x) > 0 ? (x) : -(x))
# define SIGN(x) ((x) < 0 ? (-1) : (1))
# define WSPACE (" \t\n\f\v\r")
# define FT_ENTRY(value, name, msg) {value, name, msg}

typedef struct stat	t_stat;

typedef struct		s_err_info
{
	int		value;
	char	*name;
	char	*msg;
}					t_err_info;

void				*ft_memset(void *block, int value, size_t len);
void				*ft_memcpy(void *dst, const void *src, size_t len);
int					ft_memcount(void *mem, int value, size_t len);
void				*ft_memccpy(void *dst, const void *src, int c, size_t len);
void				*ft_memchr(const void *s, int value, size_t len);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void				ft_bzero(void *s, size_t len);

size_t				ft_strlen(const char *str);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strchr(const char *str, int c);
char				*ft_strrchr(const char *str, int c);
char				*ft_strstr(const char *s1, const char *s2);

char				*ft_strdup(const char *str);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncat(char *dest, const char *src, size_t n);
char				*ft_strncpy(char *dest, const char *src, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strnew(size_t size);
void				ft_strdel(char **ap);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(const char *s, char (*f)(char));
char				*ft_strmapi(const char *s, char (*f)(unsigned int, char));
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(const char *s1, const char *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				**ft_strsplitw(char const *s);
void				ft_splitfree(char **tab);

int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_ishex(int c);
int					ft_isoct(int c);
int					ft_iswhite(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_is(int c, const char *charset);
int					ft_toupper(int c);
int					ft_tolower(int c);

int					ft_atoi(const char *str);
int					ft_basetoi(const char *str, const char *base);
int					ft_indexof(int c, const char *str);
char				*ft_itoa(int n);

void				ft_putchar(char c);
void				ft_putstr(const char *s);
void				ft_putendl(const char *s);
void				ft_putnbr(long long int n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(const char *s, int fd);
void				ft_putendl_fd(const char *s, int fd);
void				ft_putnbr_fd(long long int n, int fd);

char				*ft_skip(char *str, char *charset);
char				*ft_firstoccur(const char *str, const char *charset);

unsigned long int	swap_endian_64(unsigned long int num);
unsigned int		swap_endian_32(unsigned int num);
unsigned short		swap_endian_16(unsigned short num);

int					ft_nbrlen(long long int n);
int					ft_nbrlen_base(long long int n, int base);
int					ft_putnbr_base(unsigned long int n,
					char *charset, unsigned int base);

char				*ft_strerror(int errnum);

char				*ft_getfile(int fd);

#endif
