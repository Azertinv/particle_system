/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/09 18:37:59 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:33:06 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	char			*return_ptr;
	unsigned int	i;

	if (!dest || !src)
		return (NULL);
	return_ptr = dest;
	while (*dest)
		dest++;
	i = 0;
	while (*src && i < n)
	{
		*dest = *src;
		i++;
		dest++;
		src++;
	}
	*dest = '\0';
	return (return_ptr);
}
