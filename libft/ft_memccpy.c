/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 14:11:53 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:04:09 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	uc_c;
	size_t			i;
	void			*ret;

	if (!dst || !src)
		return (NULL);
	ret = NULL;
	uc_c = (unsigned char)c;
	i = 0;
	while (i < n && *((unsigned char *)src + i) != uc_c)
	{
		*((unsigned char *)dst + i) = *((unsigned char *)src + i);
		i++;
	}
	if (i < n && *((unsigned char *)src + i) == uc_c)
	{
		*((unsigned char *)dst + i) = *((unsigned char *)src + i);
		return (dst + i + 1);
	}
	return (ret);
}
