/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_firstoccur.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 21:11:31 by larrive           #+#    #+#             */
/*   Updated: 2015/01/31 21:38:10 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_firstoccur(const char *str, const char *charset)
{
	char	*ret;
	char	*tmp;

	ret = NULL;
	while (*charset)
	{
		if ((tmp = ft_strchr(str, *charset)) && (!ret || tmp < ret))
			ret = tmp;
		charset++;
	}
	return ((char *)ret);
}
