/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:56:05 by larrive           #+#    #+#             */
/*   Updated: 2014/11/15 17:23:57 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	split_tab_size(const char *s, char c)
{
	while (*s && *s == c)
		s++;
	if (!*s)
		return (0);
	while (*s && *s != c)
		s++;
	return (1 + split_tab_size(s, c));
}

static size_t	split_elem_size(const char *s, char c)
{
	size_t	size;

	size = 0;
	while (*s && *s != c)
	{
		s++;
		size++;
	}
	return (size);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**tab;
	size_t	elem_size;
	size_t	tab_len;
	size_t	i;

	if (!s)
		return (NULL);
	tab_len = split_tab_size(s, c);
	tab = (char **)malloc(sizeof(char *) * (tab_len + 1));
	if (!tab)
		return (NULL);
	i = 0;
	while (i < tab_len)
	{
		while (*s && *s == c)
			s++;
		elem_size = split_elem_size(s, c);
		tab[i] = ft_memcpy(ft_strnew(elem_size), s, elem_size);
		s += elem_size + 1;
		i++;
	}
	tab[tab_len] = NULL;
	return (tab);
}
