/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/05 13:55:07 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:36:24 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		result;
	int		negative;

	if (!str)
		return (0);
	negative = 0;
	result = 0;
	while (*str && (*str == ' ' || *str == '\f' || *str == '\t' ||
				*str == '\v' || *str == '\r' || *str == '\n'))
		str++;
	if (*str == '-')
		negative = (long int)str++;
	else if (*str == '+')
		str++;
	while (*str && *str >= '0' && *str <= '9')
	{
		result *= 10;
		result += *str - '0';
		str++;
	}
	if (negative)
		result = -result;
	return (result);
}
