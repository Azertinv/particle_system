/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 15:44:39 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 17:04:39 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int value, size_t len)
{
	unsigned int	i;
	char			*ret;

	if (!s)
		return (NULL);
	ret = NULL;
	i = 0;
	while (i < len && *((unsigned char *)s + i) != (unsigned char)value)
		i++;
	if (i != len)
		ret = (char *)s + i;
	return (ret);
}
