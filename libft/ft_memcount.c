/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcount.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/02 22:51:30 by larrive           #+#    #+#             */
/*   Updated: 2015/03/02 22:56:04 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcount(void *mem, int value, size_t len)
{
	int		count;
	size_t	i;

	i = -1;
	count = 0;
	while (++i < len)
		if (((char *)mem)[i] == (char)value)
			count++;
	return (count);
}
