/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:42:25 by larrive           #+#    #+#             */
/*   Updated: 2015/02/04 19:28:25 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*last_carac;
	size_t	i;

	s = ft_skip((char *)s, WSPACE);
	i = 0;
	last_carac = (char *)s + i;
	while (s[i])
	{
		if (ft_isprint(s[i]) && s[i] != ' ')
			last_carac = (char *)s + i;
		i++;
	}
	return (ft_strsub(s, 0, last_carac - s + 1));
}
