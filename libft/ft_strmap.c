/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 13:31:15 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:35:30 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(const char *s, char (*f)(char))
{
	char	*ret;
	size_t	i;

	if (!s || !f)
		return (NULL);
	ret = ft_strdup(s);
	i = 0;
	while (ret[i])
	{
		ret[i] = f(ret[i]);
		i++;
	}
	return (ret);
}
