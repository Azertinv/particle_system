/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getfile.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 09:23:39 by larrive           #+#    #+#             */
/*   Updated: 2015/10/14 09:31:34 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_getfile(int fd)
{
	t_stat	stat;
	char	*str;

	if (fstat(fd, &stat) == -1)
		return (NULL);
	str = (char *)malloc(stat.st_size + 1);
	read(fd, str, stat.st_size);
	str[stat.st_size] = '\0';
	return (str);
}
