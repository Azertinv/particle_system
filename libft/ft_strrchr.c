/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 18:32:41 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:30:54 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	char	*ret;

	if (!str)
		return (NULL);
	ret = NULL;
	while (*str)
	{
		if (*str == (char)c)
			ret = (char *)str;
		str++;
	}
	if (*str == (char)c)
		ret = (char *)str;
	return (ret);
}
