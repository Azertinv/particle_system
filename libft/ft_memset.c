/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 12:34:23 by larrive           #+#    #+#             */
/*   Updated: 2014/12/29 18:28:19 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *block, int value, size_t len)
{
	unsigned char	uc_value;
	unsigned int	i;

	if (!block)
		return (NULL);
	uc_value = (unsigned char)value;
	i = 0;
	while (i < len)
	{
		*((unsigned char *)block + i) = uc_value;
		i++;
	}
	return (block);
}
