/***************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    sph.cl                                             :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: larrive <larrive@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/13 13:51:32 by larrive           #+#    #+#              #
#    Updated: 2016/03/30 14:23:16 by larrive          ###   ########.fr        #
#                                                                              #
# *****************************************************************************/

ulong xorshift64star(ulong *x) {
	*x ^= *x >> 12; // a
	*x ^= *x << 25; // b
	*x ^= *x >> 27; // c
	return (*x * 2685821657736338717);
}

float3	get_color(float dist)
{
	float	H = fmod(dist * 10, 360) / 60;
	float	X = 1 - fabs(fmod(H, 2) - 1);
	if (H < 1)
		return ((float3)(1, X, 0));
	else if (H < 2)
		return ((float3)(X, 1, 0));
	else if (H < 3)
		return ((float3)(0, 1, X));
	else if (H < 4)
		return ((float3)(0, X, 1));
	else if (H < 5)
		return ((float3)(X, 0, 1));
	else if (H < 6)
		return ((float3)(1, 0, X));
	return ((float3)(1, 1, 1));
}

__kernel void	particle_system(
	global read_write float3	*pos,
	global read_write float3	*vel,
	global read_write float3	*color,
	int					 		count,
	float						delta,
	float3						external,
	int							running,
	int							grav_state,
	float						speed)
{
	float3	acc_grav = 0;

	float	G = 30;
	int		id = get_global_id(0);
	delta *= speed;

	if (id < count)
	{
		color[id] = get_color(fast_length(external - pos[id]));
		if (running)
		{
			if (grav_state == 1)
				acc_grav = normalize(external - pos[id]) * G;
			else if (grav_state == 2)
				acc_grav = normalize(external - pos[id]) * G *
					(1 / pow(fast_length(external - pos[id]), 2));
			vel[id] += (acc_grav) * delta;
			pos[id] += vel[id] * delta;
		}
	}
}

__kernel void	particle_initc(
	global read_write float3	*pos,
	global read_write float3	*vel,
	global read_write float3	*color,
	int							count)
{
	int		id = get_global_id(0);

	if (id < count)
	{
		ulong	state = id;
		float	x = (float)xorshift64star(&state) / pow(2.0, 64.0) * 10.0 - 5.0;
		float	y = (float)xorshift64star(&state) / pow(2.0, 64.0) * 10.0 - 5.0;
		float	z = (float)xorshift64star(&state) / pow(2.0, 64.0) * 10.0 - 5.0;
		pos[id] = (float3)(x, y, z);
		vel[id] = (float3)(0, 0, 0);
		color[id] = (float3)(1, 1, 1);
	}
}

__kernel void	particle_inits(
	global read_write float3	*pos,
	global read_write float3	*vel,
	global read_write float3	*color,
	int							count)
{
	int		id = get_global_id(0);

	if (id < count)
	{
		ulong	state = id;
		float	x = sin((float)xorshift64star(&state) / pow(2.0, 64.0) * M_PI * 2);
		float	y = -tan((float)xorshift64star(&state) / pow(2.0, 64.0) * M_PI * 2);
		float	z = -cos((float)xorshift64star(&state) / pow(2.0, 64.0) * M_PI * 2);
		pos[id] = normalize((float3)(x, y, z)) *
			(float3)((float)xorshift64star(&state) / pow(2.0, 64.0) * 6);
		vel[id] = (float3)(0, 0, 0);
		color[id] = (float3)(1, 1, 1);
	}
}

__kernel void	particle_add(
	global read_write float3	*pos,
	global read_write float3	*vel,
	global read_write float3	*color,
	int							count,
	float3						external,
	int							amount)
{
	int		id = get_global_id(0);
	
	if (id >= count && id < count + amount)
	{
		ulong	state = id;
		float	x = (float)xorshift64star(&state) / pow(2.0, 64.0) * 4.0 - 2.0;
		float	y = (float)xorshift64star(&state) / pow(2.0, 64.0) * 4.0 - 2.0;
		float	z = (float)xorshift64star(&state) / pow(2.0, 64.0) * 4.0 - 2.0;
		pos[id] = (float3)(x, y, z) + external;
		x = (float)xorshift64star(&state) / pow(2.0, 64.0) * 2.0 - 1.0;
		y = (float)xorshift64star(&state) / pow(2.0, 64.0) * 2.0 - 1.0;
		z = (float)xorshift64star(&state) / pow(2.0, 64.0) * 2.0 - 1.0;
		vel[id] = (float3)(x, y, z);
		color[id] = (float3)(0, 0, 1);
	}
}
