# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: larrive <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/07/15 10:51:27 by larrive           #+#    #+#              #
#    Updated: 2016/02/21 17:12:50 by larrive          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = particle_system
SRC = main.c shader.c opengl.c ft_matrix.c ft_matrix_gen.c projection.c \
	  camera.c scene.c opencl.c
HD = ./src/particle.h ./libft/libft.h ./src/ft_matrix.h
OBJ = $(SRC:.c=.o)
GLFW = $(HOME)/lib/libglfw3.a
GLEW = $(HOME)/lib/libGLEW.a
CFLAG = -Wall -Wextra -Werror -O3 -I$(HOME)/include -I./libft -g
LDFLAG = -lm -L$(HOME)/lib -lglfw3 -lGLEW -L./libft -lft -framework OpenGL \
		 -framework AppKit -framework IOKit -framework CoreVideo \
		 -framework OpenCL -g
CC = gcc

all: $(NAME)

$(NAME): $(OBJ)
	make -C ./libft -j 8
	$(CC) $(LDFLAG) $(OBJ) -o $(NAME)

%.o: ./src/%.c $(HD)
	$(CC) $(CFLAG) -o $@ -c $<

$(GLEW):
	mkdir /tmp/$(NAME)
	git clone https://github.com/nigels-com/glew.git /tmp/$(NAME)/glew
	cd /tmp/$(NAME)/glew
	make GLEW_DEST=$(HOME) GLEW_PREFIX=$(HOME)
	make GLEW_DEST=$(HOME) GLEW_PREFIX=$(HOME) install
	cd -
	rm -rf /tmp/$(NAME)

$(GLFW):
	mkdir /tmp/$(NAME)
	git clone https://github.com/glfw/glfw.git /tmp/$(NAME)/glfw
	cd /tmp/$(NAME)/glfw
	cmake -DCMAKE_INSTALL_PREFIX:PATH=$(HOME) .
	cmake -DCMAKE_INSTALL_PREFIX:PATH=$(HOME) .
	make
	make install
	cd -
	rm -rf /tmp/$(NAME)

.PHONY: all clean fclean re

clean:
	make -C ./libft clean
	rm -rf $(OBJ)

fclean: clean
	make -C ./libft fclean
	rm -rf $(NAME)

re: fclean all
